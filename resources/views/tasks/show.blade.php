@extends('layouts.app')
@section('content')
    <h1>{{ $task->title }}</h1>
    <h3>{{ $task->description }}</h3>

    <a href="/task">Back</a>
    <a href="/task/{{ $task->id }}/edit">Edit</a>

@endsection