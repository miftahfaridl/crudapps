
        <label for="">Title</label>
        <input type="text" name="title" id="title" value="{{ old('title') ?? $task->title }}">
        <br>
        <label for="">Description</label>
        <textarea name="description" id="description" rows="10" >{{ old('description') ?? $task->description }}</textarea>
        <br>
        <button type="submit">Save</button>