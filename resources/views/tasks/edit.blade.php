@extends('layouts.app')
@section('content')
    <h1>Task</h1>
    <h1>Edit Task : <a href="/task/{{$ task->id }}">{{ $task->title }}</a></h1>

    @foreach ($errors->all() as $error)
        <p>{{ $errors->first() }}</p>
    @endforeach

    <form action="/task" method="post" autocomplete="off">
        @csrf

        @include('tasks.partials.form')

    </form>

@endsection