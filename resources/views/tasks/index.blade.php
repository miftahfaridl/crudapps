@extends('layouts.app')
@section('content')
    <h1>Task</h1>

    <ol>
    @foreach($tasks as $task)
        <li>
            <a href="/task/{{ $task->id }}">{{ $task->title }}</a>
        </li>
    @endforeach
    </ol>

    @foreach ($errors->all() as $error)
        <p>{{ $errors->first() }}</p>
    @endforeach

    <form action="/task" method="post" autocomplete="off">
        @csrf

        @include('tasks.partials.form',[
            'task' => new App\Task;
        ])

    </form>

@endsection