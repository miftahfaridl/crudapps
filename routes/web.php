<?php

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', 'HomeController@index');

Route::get('task', 'TaskController@index');

Route::post('task', 'TaskController@store');

Route::get('task/{task}', 'TaskController@show');

Route::get('task/{task}/edit', 'TaskController@edit');