<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Task;

class TaskController extends Controller
{
    public function index()
    {
        $tasks = Task::all();
        return view('tasks.index', compact('tasks'));
    }

    public function store()
    {
        // $task = new Task;
        // $task->title = request('title');
        // $task->description = request('description');
        // $task->save();
        request()->validate([
            'title' => 'required|min:3|max:100',
            'description' => 'required'
        ]);


        $task = request()->all();
        Task::create($task);

        return back();
    }

    public function show(Task $task) // Route Model Binding (Task $task)
    {
        // $task = Task::find($task);
        return view('tasks.show', compact('task'));
    }

    public function edit(Task $task)
    {
        return view('tasks.edit', compact('task'));
    }
}
